﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyFlip : MonoBehaviour
{
    public AIPath aiPath;
    public GameObject player;
    public float distance;

    // Update is called once per frame
    void Update()
    {
        if(aiPath.desiredVelocity.x >= 0.01f)
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
        }
        else if (aiPath.desiredVelocity.x <= -0.01f)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }

        if(Vector3.Distance(player.transform.position, transform.position) < distance)
        {
            aiPath.canMove = true;
        }
        else
        {
            aiPath.canMove = false;
        }
    }
}
