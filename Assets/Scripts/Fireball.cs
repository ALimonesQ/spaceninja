﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    public GameObject particulas;
    public float speed = 3.0f;

    void FixedUpdate()
    {
        transform.position += -transform.right * speed * Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        Instantiate(particulas, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
