﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretRC : MonoBehaviour
{
    [SerializeField]
    Transform castPoint1, castPoint2;
    public GameObject Fireball;
    public float timer;
    public float waitingTime;
    public int k = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    /*void Update()
    {
        
    }*/

    void FixedUpdate()
    {
        //Length of the ray
        float laserLength = 45f;
        //Get the first object hit by the ray
        RaycastHit2D hit = Physics2D.Raycast(castPoint1.position, Vector2.left, laserLength);
    
        //If the collider of the object hit is not NUll
        if (hit.collider != null)
        {
            if (hit.collider.tag == "Player")
            {
                //Hit something, print the tag of the object
                Debug.Log("Hitting: " + hit.collider.tag);
                //Method to draw the ray in scene for debug purpose
                Debug.DrawRay(castPoint1.position, Vector2.left * (castPoint1.position.x - hit.point.x), Color.red);
                    timer += Time.deltaTime;
                    if(timer > waitingTime){
                        if(k < 4){
                            Instantiate(Fireball, castPoint2.position, Quaternion.identity);
                            timer = 0;
                            k++;
                        }
                        else{
                            timer = 0;
                            k = 0;
                        }
                    }
            }
        }
    }
    
}
