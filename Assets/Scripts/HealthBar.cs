﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
  public Slider hbar;

  // Start is called before the first frame update
  public void sethp(int hp) {
    hbar.value = hp;
  }

  public void setmaxhp(int x) {
    hbar.maxValue = x;
    hbar.value = x;
  }
    
}
