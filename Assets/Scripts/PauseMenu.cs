﻿using System;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public GameObject button1, button2;
    bool paused = false;
    public AudioSource pauseIn, pauseOut;
    public AudioSource musica;

    void Start() { 
        //button1.SetActive(false);
        //button2.SetActive(false);
        musica = GameObject.Find("Main Camera").GetComponent<AudioSource>();
    }

    void Update()
    {
        if(Input.GetButtonDown("pause"))
            paused = togglePause();
    }

    /*void OnGUI()
    {
        if(paused)
        {
            GUILayout.Label("Game is paused!");
            if(GUILayout.Button("Click me to unpause"))
                paused = togglePause();
        }
    }*/

    bool togglePause()
    {
        if(Time.timeScale == 0f)
        {
            Time.timeScale = 1f;
            button1.SetActive(false);
            button2.SetActive(false);
            musica.volume = musica.volume * 4;
            pauseOut.Play();
            return(false);
        }
        else
        {
            Time.timeScale = 0f;
            button1.SetActive(true);
            button2.SetActive(true);
            musica.volume = musica.volume / 4;
            pauseIn.Play();
            return(true);
        }
    }
}