﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AccionesNinja : MonoBehaviour
{
    Animator ninjaAnim;
    public Rigidbody2D rb;
    public float launchForce;
    public AudioSource bounce, gas, hit, heal, dead, gameover, success;
    public int hp, maxhp = 3;
    public HealthBar hbar;
    public Text gasCounter, mensajeGrande;
    public int gasCount = 0;
    public bool golpeado;
    public GameObject button1, button2;
    public AudioSource musica;
    public GameObject particulas;
    //public Animator anim;

    
    // Start is called before the first frame update
    void Start()
    {
        ninjaAnim = this.GetComponent<Animator>();
        //bounce = GetComponent<AudioSource>();
        hp = maxhp;
        hbar.setmaxhp(maxhp);
        gasCounter = GameObject.Find("gasCount").GetComponent<Text>();
        mensajeGrande = GameObject.Find("mensajeGrande").GetComponent<Text>();
        musica = GameObject.Find("Main Camera").GetComponent<AudioSource>();
        //anim = GetComponent<Animator>("ShiroiHit");
    }

    // Update is called once per frame
    void Update()
    {
        gasCounter.text = gasCount + "/15";
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.CompareTag("bouncepad")){
            rb.velocity = Vector2.up * launchForce;
            bounce.volume = 0.1f;
            bounce.Play();
        }
        if(other.gameObject.CompareTag("Alien")){ 
            //StartCoroutine(damage());
            damage();
        }
        if(other.gameObject.CompareTag("Gas")){
            Destroy(other.gameObject);    
            gasCount++;
            //gasCounter.text = gasCount + "/15";
            gas.volume = 0.1f;
            gas.Play();
        }
        if(other.gameObject.CompareTag("Nave")){
            if(gasCount == 15){
                mensajeGrande.text = "NIVEL COMPLETADO!";
                Pause(true);
                button1.SetActive(true);
                button2.SetActive(true);
                success.volume = 0.06f;
                musica.volume = musica.volume / 8;
                success.Play();
            } else {
                StartCoroutine(MissingGas());
            }
        }
        if(other.gameObject.CompareTag("Heal")){
            Destroy(other.gameObject);
            if(hp < maxhp){
                hp++;
                hbar.sethp(hp);
            }
            heal.volume = 0.1f;
            heal.Play();
        }
        if(other.gameObject.CompareTag("tp")){
            transform.position = new Vector2(210, 0);
        }
        if(other.gameObject.CompareTag("Suelo")){
            while(hp > 0){
                damage();
            }
        }
    }

    void damage()
    {
        //anim.play("ShiroiHit");
        hp--;
        hbar.sethp(hp);
        if (hp == 0)
        {
            StartCoroutine(GameOver());
            /*if(Input.GetKeyDown(KeyCode.Space)){
                Debug.Log("espacio");
                SceneManager.LoadScene(2);
            }*/
        }
        else{
            ninjaAnim.SetTrigger("takeDmg");
            hit.volume = 0.1f;
            hit.Play();
        }
    }

    void Pause(bool x) {
        if (x) {
            Time.timeScale = 0;
        } else {
            Time.timeScale = 1;
        }
    }

    IEnumerator MissingGas()
    {
        transform.position = new Vector2(-8.105802f, -2.568905f);
        mensajeGrande.text = "FALTA GASOLINA!";
        yield return new WaitForSeconds(4.0f);
        mensajeGrande.text = "";
    }

    IEnumerator GameOver()
    {
        Instantiate(particulas, transform.position, Quaternion.identity);
        dead.volume = 0.1f;
        dead.Play();
        musica.volume = 0;
        this.GetComponent<Renderer>().enabled = false;
        yield return new WaitForSeconds(2.0f);
        Pause(true);
        mensajeGrande.text = "GAME OVER!";
        gameover.volume = 0.06f;
        gameover.Play();
        button1.SetActive(true);
        button2.SetActive(true);
    }
}
