﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneSwitch : MonoBehaviour
{

    public void SceneSwitcherTo0 () {
        SceneManager.LoadScene(0);
        Pause(false);
    }
    public void SceneSwitcherTo1 () {
        SceneManager.LoadScene(1);
        Pause(false);
    }
    public void SceneSwitcherTo2 () {
        SceneManager.LoadScene(2);
        Pause(false);
    }
    public void SceneSwitcherTo3 () {
        SceneManager.LoadScene(3);
        Pause(false);
    }
    public void Exit () {
        Debug.Log("Has quit game");
        Application.Quit();
    }


    void Pause(bool x) {
        if (x) {
            Time.timeScale = 0;
        } else {
            Time.timeScale = 1;
        }
    }


}