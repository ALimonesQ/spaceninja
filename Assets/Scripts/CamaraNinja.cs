﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraNinja : MonoBehaviour
 {
    public Transform FollowObject;
 
    // Use this for initialization
    void Start () {
     
    }
     
    // Update is called once per frame
    void Update ()
    {
        Vector3 pos = new Vector3(FollowObject.position.x+5, transform.position.y, transform.position.z);
        transform.position = pos;
    }
 }


/*
public class CamaraNinja : MonoBehaviour {

//public GameObject Ninja;
private Vector3 posicionRelativa;

public Transform Ninja;

// Use this for initialization
void Start () {

//posicionRelativa = transform.position - Ninja.transform.position;

}

void Update (){

    transform.position = new Vector3 (Ninja.position.x + 6, 0, -10); // Camera follows the Ninja but 6 to the right
}

// Update is called once per frame
void LateUpdate () {

transform.position = Ninja.transform.position + posicionRelativa;

}
}
*/