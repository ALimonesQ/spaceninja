﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoNinja : MonoBehaviour {

    public float moveSpeed = 5f;
    public float jumpForce = 8f;
    public bool isGrounded = false;
    Animator ninjaAnim;
    SpriteRenderer ninjaRender;
    public bool atacando = false;
    bool voltearNinja = true;
    public Transform attackPoint1, attackPoint2;
    public bool derecha = true;
    public float range = 0.5f;
    public LayerMask enemyLayers;
    public AudioSource sword, jump;
    public AudioClip[] otherClip;
    public GameObject particulas;


    void Start(){
        ninjaRender = GetComponent<SpriteRenderer>();
        ninjaAnim = GetComponent<Animator>();
        //sword = GetComponent<AudioSource>();
    }

    void Update(){
        float mover = Input.GetAxis("Horizontal");
        Jump();
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
        transform.position += movement * Time.deltaTime * moveSpeed;
        if(mover > 0 && !voltearNinja && !atacando){
            Voltear();
        }else if (mover < 0 && voltearNinja && !atacando){
            Voltear();
        }

        //Animacion correr ninja
        ninjaAnim.SetFloat("VelMovimiento", Mathf.Abs(mover));
        Espada();
    }

    void Jump(){
        if(Input.GetButtonDown("Jump") && isGrounded == true){
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
            jump.volume = 0.1f;
            jump.Play();
        }
    }

    void Espada(){
        if(Input.GetButtonDown("Espada") && isGrounded == true && atacando == false){
            atacando = true;
            ninjaAnim.SetBool("atacar", true);
            StartCoroutine(ResetAtacando());
            if(derecha && atacando == true){
                Collider2D[] hit = Physics2D.OverlapCircleAll(attackPoint1.position, range, enemyLayers);
                foreach (Collider2D enemy in hit){
                    Instantiate(particulas, enemy.transform.position, Quaternion.identity);
                    Destroy(enemy.gameObject);
                }
            }
            else if(derecha == false && atacando == true){
                Collider2D[] hit = Physics2D.OverlapCircleAll(attackPoint2.position, range, enemyLayers);
                foreach (Collider2D enemy in hit){
                    Instantiate(particulas, enemy.transform.position, Quaternion.identity);
                    Destroy(enemy.gameObject);
                }
            }
            sword.Play();
        }
    }

    void Voltear(){
        voltearNinja = !voltearNinja;
        ninjaRender.flipX = !ninjaRender.flipX;
        if(derecha)
            derecha = false;
        else
            derecha = true;
    }

    IEnumerator ResetAtacando(){
        yield return new WaitForSeconds(0.5f);
        atacando = false;
        ninjaAnim.SetBool("atacar", false);
    }
}