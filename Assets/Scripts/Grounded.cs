﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grounded : MonoBehaviour
{
    GameObject Ninja;
    Animator ninjaAnim;
    // Start is called before the first frame update
    void Start()
    {
        Ninja = GameObject.FindGameObjectWithTag("Player");
        ninjaAnim = Ninja.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D other)
        {
        if (other.tag == "GroundCheck"){
            Ninja.GetComponent<MovimientoNinja>().isGrounded = true;
            ninjaAnim.SetBool("Tierra", true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "GroundCheck"){
            Ninja.GetComponent<MovimientoNinja>().isGrounded = false;
            ninjaAnim.SetBool("Tierra", false);
        }
    }
}
