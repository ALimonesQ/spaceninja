﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringAnim : MonoBehaviour
{
    Animator springAnim;

    void Start()
    {
        springAnim = GetComponent<Animator>();
        springAnim.speed = 0;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            StartCoroutine(Anim());
        }
    }

    IEnumerator Anim()
    {
        springAnim.speed = 1;
        yield return new WaitForSeconds(1.0f);
        springAnim.speed = 0;
    }
}
